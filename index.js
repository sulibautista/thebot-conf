"use strict";

module.exports = require('config');

let conf = Object.create(require('config'), {
  redis: {
    value: require('./lib/redis.js'),
  },
  log: {
    value: require('./lib/logger.js')
  }
});

module.exports = conf;