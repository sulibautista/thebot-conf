"use strict";

const
  _ = require('lodash'),
  bunyan = require("bunyan");  

let
  loggers = {},
  defaultConf = {
    stream: process.stdout,
    level: 'info',
    serializers: {
      creq: function(req){
        if(!req){
          return req;
        }

        return {
          id: req.id,
          args: req.args,
          userName: req.userName,
          channelName: req.channelName,
          channelId: req.channelId,
          userId: req.userId
        }
      },

      cres: function(res){
        if(!res){
          return res;
        }

        let req = res.req || {};

        return _.extend(_.omit(res, 'req'), {
          channelId: req.channelId,
          userId: req.userId,
          channelState: req.channel && req.channel.state,
          userAuth: req.channelUser && req.channelUser.groups
        });
      }
    }
  };
  
if(process.env.NODE_ENV === 'dev'){
  const PrettyStream = require('bunyan-prettystream');
  defaultConf.stream = new PrettyStream();
  defaultConf.stream.pipe(process.stdout);
  defaultConf.level = 'debug';
}

module.exports = function(profile, conf){
  if(!profile){
    profile = 'default';
  }
  
  if(loggers[profile]){
    return loggers[profile];
  }
  
  let logger = bunyan.createLogger(_.defaults(conf, defaultConf));
  
  loggers[profile] = logger;
  
  return logger;
};