"use strict";

const
  redis = require('redis'),
  config = require('config');
  
let connections = {};

module.exports = function(profile){
  profile = profile || 'default';
  
  if(connections[profile]){
    return connections[profile];
  }
  
  let client = redis.createClient(config.get('redis.port'), config.get('redis.host'));
  if(config.has('redis.password')){
    client.auth(config.get('redis.password'));
  }
    
  connections[profile] = client;
  
  return client;
};